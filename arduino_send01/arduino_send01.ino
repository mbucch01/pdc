/*

  irSend sketch

  this code needs an IR LED connected to pin 3

  and 5 switches connected to pins 4 - 8

*/

#include <IRremote.h>       // IR remote control library

const int numberOfKeys = 5;

int value;

boolean buttonState[numberOfKeys];

boolean lastButtonState[numberOfKeys];

long irKeyCodes[numberOfKeys] = {

  0x18E758A7,  //0 key

  0x18E708F7,  //1 key

  0x18E78877,  //2 key

  0x18E748B7,  //3 key

  0x18E7C837,  //4 key

};

IRsend irsend;

void setup()

{

 

  Serial.begin(9600);

}

void loop() {

 if( Serial.available())

  {

    char ch = Serial.read();

    if( isDigit(ch) )// is this an ascii digit between 0 and 9?

    {
       value = (value * 10) + (ch - '0'); // yes, accumulate the value
    }

    else if (ch == 10)  // is the character the newline character?
    {
      Serial.print("Sending...");
       Serial.println(value);
       irsend.sendSony(irKeyCodes[value], 32); //the sendSony command may have to be revised depending on the type of remote
       Serial.println(irKeyCodes[value], HEX);
       value = 0; // reset val to 0 ready for the next sequence of digits
    }
  } 

}

