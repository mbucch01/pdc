/*

  irSend sketch

  this code needs an IR LED connected to pin 3

  and 5 switches connected to pins 4 - 8

*/

#include <IRremote.h>       // IR remote control library

const int numberOfKeys = 19;

int value;
int length;

// the following are all timer variables

int category = 0;

int time = millis();
int lastTime = 0;
int seconds0;
int seconds = seconds0/1000;
int seconds_t;
int seconds_h;
int seconds_o;
int thousands;
int hundreds;
int tens;
int ones;


const int numberOfCodes = 13;
int codeNumber;
long sendCodes[numberOfCodes] = {0,10,11,8,1,13,14,8,7,17,18,9,8};

long irKeyCodes[numberOfKeys] = {

  0x18E758A7,  //Design Problem

  0x18E708F7,  //Research

  0x18E78877,  //Brainstorm

  0x18E748B7,  //Select

  0x18E7C837,  //Construct
  
   0x18E7C8E7,  //Test/Evaluate
  
   0x18E718A7,  //Communicate
   
    0x18E728A7,  //Redesign
    
      0x18E768B7, // new line
    
    0x18E738A7, //from here down are all numbers; this is 0
    
     0x18E748A7,
     
    0x18E768A7,
    
     0x18E778A7,
       
     0x18E788A7,
     
      0x18E798A7,
      
       0x18E718B7,
       
        0x18E728B7,
        
         0x18E738B7,
         
          0x18E758B7,
    
};

int inPin = 7;   // choose the input pin (for a pushbutton)
int val = 0;     // variable for reading the pin status


IRsend irsend;

void setup()

{

 pinMode(13, OUTPUT);  
 
  pinMode(inPin, INPUT);    // declare pushbutton as input

  Serial.begin(9600);

}

void loop() {
  
  val = digitalRead(inPin);  // read input value
  if (val == LOW) {         // check if the input is HIGH (button released)
    sendTimerData(); 
   delay(2000); 
 
  } 

  
  } 

void sendSampleData() {
  
for (int i=0; i<numberOfCodes; i++)  // is the character the newline character?
    {
      codeNumber = sendCodes[i];
       irsend.sendSony(irKeyCodes[codeNumber], 32);
       Serial.println(irKeyCodes[codeNumber], HEX);
       delay(50);
  
       //sends an IR code to teensy every 2 seconds
       
    }  
}


void sendTimerData() {
  
  time = millis();
    seconds0 = time-lastTime;
    seconds = seconds0/10;
    lastTime = seconds0;
    
   if (seconds >= 1000) {
    seconds_t = seconds/1000;
    for (int i=0; i<11; i++) {
      if (seconds_t < i) {
        thousands = i-1;
        break;
      }
    } 
    
    seconds = seconds - thousands*1000;
  }
  
 if (seconds >= 100) {
    seconds_h = seconds/100;
    for (int i=0; i<11; i++) {
      if (seconds_h < i) {
        hundreds = i-1;
        break;
      }
    } 
    
    seconds = seconds - hundreds*100;
  }
  
  if (seconds >= 10) {
    seconds_t = seconds/10;
    for (int i=0; i<11; i++) {
      if (seconds_t < i) {
        tens = i-1;
        break;
      }
    } 
    
    seconds = seconds - tens*10;
  }
   
   ones = seconds; 
    
irsend.sendSony(irKeyCodes[category], 32);
       Serial.println(irKeyCodes[category], HEX);
       delay(50);
       
       if (thousands != 0) {
       irsend.sendSony(irKeyCodes[thousands + 9], 32);
       Serial.println(thousands);
       delay(50);
       }
       
       if (hundreds != 0) {
       irsend.sendSony(irKeyCodes[hundreds + 9], 32);
       Serial.println(hundreds);
       delay(50);
       }
       
       if (tens != 0) {
       irsend.sendSony(irKeyCodes[tens + 9], 32);
       Serial.println(tens);
       delay(50);
       }
       
       if (ones != 0) {
       irsend.sendSony(irKeyCodes[ones + 9], 32);
       Serial.println(ones);
       delay(50);
       }
       
       irsend.sendSony(irKeyCodes[8], 32);
       
       category++;
       if (category == 8) {
         category = 0;
       }
}
