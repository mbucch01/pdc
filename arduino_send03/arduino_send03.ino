/*

  This is like the previous send sketch, but it usees a different sending protocol
  The information from this sketch will be sent in triplets: type of info, length of info, information 

*/

#include <IRremote.h>       // IR remote control library

const int numberOfKeys = 19;

int value;
int length;

// the following are all timer variables

int time = millis();
int lastTime = 0;
int seconds;
int seconds_t;
int seconds_h;
int seconds_o;
int thousands;
int hundreds;
int tens;
int ones;


const int numberOfCodes = 5;
int codeNumber;
long sendCodes[numberOfCodes] = {2,12,10,14,18};

long irKeyCodes[numberOfKeys] = {

  // type of information codes, index 0-4
  
  0x18E758A7,  //category

  0x18E708F7,  //time

  0x18E78877,  //user ID

  0x18E748B7,  //project ID
  
   0x18E7C8E7, // end of transmission
   
  //separator codes, index 5-8

  0x18E7C837,  // comma
  
  
   0x18E718A7, 
   
    0x18E728A7,
    
      0x18E768B7,
    
    0x18E738A7, //from here down are all numbers, this is 0; index 9-18
    
     0x18E748A7,
     
    0x18E768A7,
    
     0x18E778A7,
       
     0x18E788A7,
     
      0x18E798A7,
      
       0x18E718B7,
       
        0x18E728B7,
        
         0x18E738B7,
         
          0x18E758B7,
    
};

int inPin = 7;   // choose the input pin (for a pushbutton)
int val = 0;     // variable for reading the pin status


IRsend irsend;

void setup()

{

 pinMode(13, OUTPUT);  
 
  pinMode(inPin, INPUT);    // declare pushbutton as input

  Serial.begin(9600);

}

void loop() {
  
  val = digitalRead(inPin);  // read input value
  if (val == LOW) {         // check if the input is HIGH (button released)
    sendSampleData(); 
   delay(300); 
 
  } 

  
  } 

void sendSampleData() {
  
for (int i=0; i<numberOfCodes; i++)  // is the character the newline character?
    {
      codeNumber = sendCodes[i];
       irsend.sendSony(irKeyCodes[codeNumber], 32);
       Serial.println(irKeyCodes[codeNumber], HEX);
       delay(50);
  
       //sends an IR code to teensy every 2 seconds
       
    }  
}





// this function sends a sample category and the time since the last button press
//Note: the if statements cause a bug with zeros within the number (ie 202)


boolean checkLength(int timet, int timeh, int timete, int timeo) {
  
  if (timet == 0) {
    if (timeh == 0) {
      if (timete == 0) {
        if (timeo == 0) {
          return -1;
          Serial.println("Error: no time");
        } else {
          return 1;
        }
      } else {
        return 2;
      } 
    } else {
      return 3;
    }  
   } else {
    return 4;
  }
}
  


// this function sends a sample category and the time since the last button press
//Note: the if statements cause a bug with zeros within the number (ie 202)


void sendTimerData() { //sends the time in seconds since the program started running
  
  time = millis()/1000;
 seconds = time; //   seconds = time-lastTime;
    lastTime = lastTime + seconds;
    
   if (seconds >= 1000) {
    seconds_t = seconds/1000;
    for (int i=0; i<11; i++) {
      if (seconds_t < i) {
        thousands = i-1;
        break;
      }
    } 
    
    seconds = seconds - thousands*1000;
  }
  
 if (seconds >= 100) {
    seconds_h = seconds/100;
    for (int i=0; i<11; i++) {
      if (seconds_h < i) {
        hundreds = i-1;
        break;
      }
    } 
    
    seconds = seconds - hundreds*100;
  }
  
  if (seconds >= 10) {
    seconds_t = seconds/10;
    for (int i=0; i<11; i++) {
      if (seconds_t < i) {
        tens = i-1;
        break;
      }
    } 
    
    seconds = seconds - tens*10;
  }
   
   ones = seconds; 
    
irsend.sendSony(irKeyCodes[1], 32);
       Serial.println(irKeyCodes[1], HEX);
       delay(50);
       
       length = checkLength(thousands, hundreds, tens, ones);
       Serial.print("length:");
       Serial.println(length+9);
       
       irsend.sendSony(irKeyCodes[length+9], 32);
       
       delay(50);
       
       if (length == 4) {
       irsend.sendSony(irKeyCodes[thousands + 9], 32);
       Serial.println(thousands+9);
       delay(50);
       }
       
       if (length > 2) {
       irsend.sendSony(irKeyCodes[hundreds + 9], 32);
       Serial.println(hundreds+9);
       delay(50);
       }
       
       if (length > 1) {
       irsend.sendSony(irKeyCodes[tens + 9], 32);
       Serial.println(tens+9);
       delay(50);
       }
       
       if (length > 0) {
       irsend.sendSony(irKeyCodes[ones + 9], 32);
       Serial.println(ones+9);
       delay(50);
       }
       
      
      
       }


