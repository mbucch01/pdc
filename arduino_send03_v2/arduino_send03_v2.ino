/*

  This is like the previous send sketch, but it usees a different sending protocol
  The information from this sketch will be sent in triplets: type of info, length of info, information 

*/

#include <IRremote.h>       // IR remote control library
#include "timer.h"

timer time_1;

const int numberOfKeys = 19;

int value;
int length;

// the following are all timer variables

int time = millis();
int lastTime = 0;
int seconds;
int seconds_t;
int seconds_h;
int seconds_o;
int thousands;
int hundreds;
int tens;
int ones;


const int numberOfCodes = 14;
int codeNumber;
const int maxNumberOfCodes = 20; //maximum number of ID/number pairs in a transmission


long sendCodes[numberOfCodes] = {2,12,10,14,18,0,10,10,1,12,14,15,16,4};
long transmissionArray[6][maxNumberOfCodes];
int category = 0;
int h_index = 0;
int v_index = 2; //number 2-6 that gives a value its vertical location in the array

long irKeyCodes[numberOfKeys] = {

  // type of information codes, index 0-4
  
  0x18E758A7,  //category
  0x18E708F7,  //time
  0x18E78877,  //user ID
  0x18E748B7,  //project ID  
   0x18E7C8E7, // end of transmission
   
  //separator codes, index 5-8, so far no use

    0x18E7C837,  // comma
    0x18E718A7,    
    0x18E728A7,
    0x18E768B7,
    
    0x18E738A7, //from here down are all numbers, this is 0; index 9-18
    0x18E748A7,     
    0x18E768A7,    
    0x18E778A7,       
    0x18E788A7,     
    0x18E798A7,      
    0x18E718B7,       
    0x18E728B7,
    0x18E738B7,
    0x18E758B7,    
};

int inPin = 7;   // choose the input pin (for a pushbutton)
int val = 0;     // variable for reading the pin status


IRsend irsend;

void setup()

{
   
 
  pinMode(inPin, INPUT);    // declare pushbutton as input

  Serial.begin(9600);

}

void loop() {
  
  
  val = digitalRead(inPin);  // read input value
  if (val == HIGH) {         // check if the input is HIGH (button pressed)
    sendTimerData(); 
    Serial.println("button pressed");
   delay(300); 
 
  } 

  
  } 

void sendSampleData() {
  
for (int i=0; i<numberOfCodes; i++)  // is the character the newline character?
    {
      codeNumber = sendCodes[i];
       irsend.sendSony(irKeyCodes[codeNumber], 32);
       Serial.println(irKeyCodes[codeNumber], HEX);
       delay(50);
  
       //sends an IR code to teensy every 2 seconds
       
    }  
}





// this function sends a sample category and the time since the last button press
//Note: the if statements cause a bug with zeros within the number (ie 202)


int checkLength(int timet, int timeh, int timete, int timeo) {
  
  if (timet == 0) {
    if (timeh == 0) {
      if (timete == 0) {
        if (timeo == 0) {
          return 0;
          Serial.println("Error: no time");
        } else {
          return 1;
        }
      } else {
        return 2;
      } 
    } else {
      return 3;
    }  
   } else {
    return 4;
  }
}
  


// this function sends a sample category and the time since the last button press
//Note: the if statements cause a bug with zeros within the number (ie 202)


void sendTimerData() { //sends the time in seconds since the program started running
  
  time = millis()/1000;
  seconds = time-lastTime;
    lastTime = time;
    
    Serial.println(seconds);
    
   if (seconds >= 1000) {
    seconds_t = seconds/1000;
    for (int i=0; i<11; i++) {
      if (seconds_t < i) {
        thousands = i-1;
        break;
      }
    } 
    
    seconds = seconds - thousands*1000;
  }
  
 if (seconds >= 100) {
    seconds_h = seconds/100;
    for (int i=0; i<11; i++) {
      if (seconds_h < i) {
        hundreds = i-1;
        break;
      }
    } 
    
    seconds = seconds - hundreds*100;
  }
  
  if (seconds >= 10) {
    seconds_t = seconds/10;
    for (int i=0; i<11; i++) {
      if (seconds_t < i) {
        tens = i-1;
        break;
      }
    } 
    
    seconds = seconds - tens*10;
  }
   
   ones = seconds; 
    

       transmissionArray[0][h_index] = irKeyCodes[0];          // put a sample category in the array
       transmissionArray[1][h_index] = 1;
       transmissionArray[2][h_index] = irKeyCodes[category+9];
       
       h_index++;
       
       
       // store a time object in the array
       
       transmissionArray[0][h_index] = irKeyCodes[1];
       
       length = checkLength(thousands, hundreds, tens, ones);
       Serial.print("length:");
       
       
       transmissionArray[1][h_index] = length; // length is the only one that remains a number
       Serial.println(transmissionArray[1][h_index]);
       
       
       if (length == 4) {
       transmissionArray[v_index][h_index] = irKeyCodes[thousands + 9];
       v_index++;
       thousands = 0;
//       Serial.println(thousands+9);
       }
       
       if (length > 2) {
       transmissionArray[v_index][h_index] = irKeyCodes[hundreds + 9];
       v_index++;
       hundreds = 0;
//       Serial.println(hundreds+9);
       }
       
       if (length > 1) {
       transmissionArray[v_index][h_index] = irKeyCodes[tens + 9];
       v_index++;
       tens = 0;
//       Serial.println(tens+9);
       }
       
       if (length > 0) {
       transmissionArray[v_index][h_index] = irKeyCodes[ones + 9];
       ones = 0;
//       Serial.println(ones+9);
       }
       
       v_index = 2;
       category++;
       h_index++;
       
       
       if (category > 3) {
         sendArray();
       }
      
       }
       
 void sendArray() {
   
   Serial.println("about to send");
   
   for (int i=0; i<h_index; i++) {                    //i is the index of the column being sent
     irsend.sendSony(transmissionArray[0][i], 32);
     Serial.print("ID: ");
     Serial.println(transmissionArray[0][i], HEX);
     delay(50);
     int length2 = transmissionArray[1][i];
     irsend.sendSony(irKeyCodes[length2 + 9], 32);
//     Serial.print("length: ");
//     Serial.println(length2);
     delay(50);
     
     for (int j=0; j<length2; j++) {                //j is the index of the row being sent
       
       irsend.sendSony(transmissionArray[j+2][i], 32);
     Serial.print("Value: ");
     
      int code = convertCodeToKey(transmissionArray[j+2][i]);
     Serial.println(code-9);
       delay(50);
     }
   }
   
   irsend.sendSony(irKeyCodes[4], 32);            //end of transmission
   h_index = 0;
   category = 0;
 }


int convertCodeToKey(long code)
{

  for( int i=0; i < numberOfKeys; i++)
  {

    if( code == irKeyCodes[i])
    {
      return i; // found the key so return it
    }
  }
  return -1;
}

