/*

  This is like the previous send sketch, but it usees a different sending protocol
  The information from this sketch will be sent in triplets: type of info, length of info, information 

*/

#include <PDCsend.h>
#include <IRremote.h>       // IR remote control library
#include "timer.h"

timer time_1;

PDCsend myPDC;

int next_time = 0;          // only for use with button timer
int time = millis();
int lastTime = 0;

int inPin = 7;   // choose the input pin (for a pushbutton)
int val = 0;     // variable for reading the pin status
long transmissionArray[6][MAXPAIRS];

//IRsend irsend;

void setup()

{ 
  pinMode(inPin, INPUT);    // declare pushbutton as input
  Serial.begin(9600);
  for(int i=0;i<6;i++) {      // initialize transmissionArray to all zeros
    for(int j=0;j<MAXPAIRS;j++) {
        transmissionArray[i][j]=0;
      }
    }
}

void loop() {  
  val = digitalRead(inPin);  // read input value
  if (val == HIGH) {         // check if the input is HIGH (button pressed)
    buttonPressed(); 
   } 
} 


// this function sends a sample category and the time since the last button press
//Note: the if statements cause a bug with zeros within the number (ie 202)


void buttonPressed()
{
  int seconds;
   time = millis()/1000;
  seconds = time-lastTime;
    lastTime = time;
    
    Serial.println(seconds);
    
   time_1.sectionTime[next_time] = seconds;
   next_time++;
   
   if(next_time == 5) {
       myPDC.createArray(time_1.sectionTime, transmissionArray);
       myPDC.sendArray(transmissionArray);
       next_time = 0;
   }
}
