/*

  This is like the previous send sketch, but it usees a different sending protocol
  The information from this sketch will be sent in triplets: type of info, length of info, information 

*/

#include <PDCsend.h>
#include <IRremote.h>       // IR remote control library
#include "timer.h"


timer time_1;

PDCsend myPDC;

const int numberOfKeys = 19;

int next_time = 0;          // only for use with button timer
int time = millis();
int lastTime = 0;


const int number_of_times = 8;

// the following are all timer variables


int seconds;
int thousands;
int hundreds;
int tens;
int ones;


const int maxNumberOfCodes = 20; //maximum number of ID/number pairs in a transmission


long transmissionArray[6][maxNumberOfCodes];


long irKeyCodes[numberOfKeys] = {

  // type of information codes, index 0-4
  
  0x18E758A7,  //category
  0x18E708F7,  //time
  0x18E78877,  //user ID
  0x18E748B7,  //project ID  
   0x18E7C8E7, // end of transmission
   
  //separator codes, index 5-8, so far no use

    0x18E7C837,  // comma
    0x18E718A7,    
    0x18E728A7,
    0x18E768B7,
    
    0x18E738A7, //from here down are all numbers, this is 0; index 9-18
    0x18E748A7,     
    0x18E768A7,    
    0x18E778A7,       
    0x18E788A7,     
    0x18E798A7,      
    0x18E718B7,       
    0x18E728B7,
    0x18E738B7,
    0x18E758B7,    
};

int inPin = 7;   // choose the input pin (for a pushbutton)
int val = 0;     // variable for reading the pin status


IRsend irsend;

void setup()

{
   
 
  pinMode(inPin, INPUT);    // declare pushbutton as input

  Serial.begin(9600);

}

void loop() {
  
  
  val = digitalRead(inPin);  // read input value
  if (val == HIGH) {         // check if the input is HIGH (button pressed)
    buttonPressed(); 
//    Serial.println("button pressed");
   delay(300); 
 
  } 

  
  } 


// this function sends a sample category and the time since the last button press
//Note: the if statements cause a bug with zeros within the number (ie 202)


int checkLength(int timet, int timeh, int timete, int timeo) {
  
  if (timet == 0) {
    if (timeh == 0) {
      if (timete == 0) {
        if (timeo == 0) {
          return 0;
          Serial.println("Error: no time");
        } else {
          return 1;
        }
      } else {
        return 2;
      } 
    } else {
      return 3;
    }  
   } else {
    return 4;
  }
}
  


// this function sends a sample category and the time since the last button press
//Note: the if statements cause a bug with zeros within the number (ie 202)


void buttonPressed()
{
   time = millis()/1000;
  seconds = time-lastTime;
    lastTime = time;
    
    Serial.println(seconds);
    
   time_1.sectionTime[next_time] = seconds;
   next_time++;
   
   if(next_time == 5) {
       sendTimerData();
       next_time = 0;
   }
  
}

void sendTimerData() { //sends the time in seconds since the program started running
  
    createArray(time_1.sectionTime);
    sendArray();
    

      
}
       
void createArray(unsigned int* time_array) {
 
  int h_index = 0;
  int time_index = 0;
int v_index = 2; //number 2-6 that gives a value its vertical location in the array

// int numberOfColumns = 2*numberOfTimes; // twice as many columns for Categories and for Times     <-- probably won't need this
 
 

   for (int i=0; i<number_of_times; i++) {
  
       transmissionArray[0][h_index] = irKeyCodes[0];          // put a sample category in the array
       transmissionArray[1][h_index] = 1;
       transmissionArray[2][h_index] = irKeyCodes[10];          // sample category number 1
       
       h_index++;
   
       int theTime = time_array[time_index];
       breakItDown(theTime);
       int length = checkLength(thousands, hundreds, tens, ones);
       
        // store stuff in the next time column of the matrix
       transmissionArray[0][h_index] = irKeyCodes[1];        //store the ID for a time type value
       transmissionArray[1][h_index] = length;             // length is the only one that remains a number       
       
       
       
       // this next block of code stores the digits for the next time value descending in the selected column of transmissionArray
       
              if (length == 4) {
       transmissionArray[v_index][h_index] = irKeyCodes[thousands + 9];
       v_index++;
       thousands = 0;
//       Serial.println(thousands+9);
       }
       
       if (length > 2) {
       transmissionArray[v_index][h_index] = irKeyCodes[hundreds + 9];
       v_index++;
       hundreds = 0;
//       Serial.println(hundreds+9);
       }
       
       if (length > 1) {
       transmissionArray[v_index][h_index] = irKeyCodes[tens + 9];
       v_index++;
       tens = 0;
//       Serial.println(tens+9);
       }
       
       if (length > 0) {
       transmissionArray[v_index][h_index] = irKeyCodes[ones + 9];
       ones = 0;
//       Serial.println(ones+9);
       }
       
       v_index = 2;            //resets v_index, thousands, hundreds, tens, and ones for the next time through
       h_index++;
       time_index++;
       
       
         
   }

  
}

 
 
 // this function sends transmission array      
       
 void sendArray() {
   
   Serial.println("about to send");
   
   for (int i=0; i<number_of_times; i++) {                    //i is the index of the column being sent
     irsend.sendSony(transmissionArray[0][i], 32);
     Serial.print("ID: ");
     Serial.println(convertCodeToKey(transmissionArray[0][i]));
     delay(50);
     int length2 = transmissionArray[1][i];
     irsend.sendSony(irKeyCodes[length2 + 9], 32);
//     Serial.print("length: ");
//     Serial.println(length2);
     delay(50);
     
     for (int j=0; j<length2; j++) {                //j is the index of the row being sent
       
       irsend.sendSony(transmissionArray[j+2][i], 32);
     Serial.print("Value: ");
     
      int code = convertCodeToKey(transmissionArray[j+2][i]);
     Serial.println(code-9);
       delay(50);
     }
   }
   
   irsend.sendSony(irKeyCodes[4], 32);            //end of transmission


 }


// this function can convert an IR code to 


int convertCodeToKey(long code)
{

  for( int i=0; i < numberOfKeys; i++)
  {

    if( code == irKeyCodes[i])
    {
      return i; // found the key so return it
    }
  }
  return -1;
}


void breakItDown(int seconds) {      //breaks down a time value into thousands, hundreds, tens, and ones

int seconds_t;
int seconds_h;
int seconds_o;

   if (seconds >= 1000) {
    seconds_t = seconds/1000;
    for (int i=0; i<11; i++) {
      if (seconds_t < i) {
        thousands = i-1;
        break;
      }
    } 
    
    seconds = seconds - thousands*1000;
  }
  
 if (seconds >= 100) {
    seconds_h = seconds/100;
    for (int i=0; i<11; i++) {
      if (seconds_h < i) {
        hundreds = i-1;
        break;
      }
    } 
    
    seconds = seconds - hundreds*100;
  }
  
  if (seconds >= 10) {
    seconds_t = seconds/10;
    for (int i=0; i<11; i++) {
      if (seconds_t < i) {
        tens = i-1;
        break;
      }
    } 
    
    seconds = seconds - tens*10;
  }
   
   ones = seconds; 
    
}
