/*

  RemoteDecode sketch
 -Assign each button on a remote an individual number, this will be it's index in the array
 -At the beginning, this sketch will prompt you for as many keys as you specify with const int numberOfKeys
 -After setupt, the sketch can do two things
   1) Tell you which button you pressed on the same remote
   2) Imitate the signal of any of the IR codes recorded during setup, using Serial.read wiht the number assigned at the beginning
   3) Connect the code with a alphanumeric character in the translated Codes string
   
   
   Make sure serial monitor is set to newline before sending a signal with the serial monitor

 */

#include <IRremote.h>           // IR remote control library

const int irReceivePin = 10;     // pin connected to IR detector output
const int numberOfKeys = 19;     //  how many keys you want to learn

int value;

long irKeyCodes[numberOfKeys] = {

  0x18E758A7,  //Design Problem

  0x18E708F7,  //Research

  0x18E78877,  //Brainstorm

  0x18E748B7,  //Select

  0x18E7C837,  //Construct
  
   0x18E7C8E7,  //Test/Evaluate
  
   0x18E718A7,  //Communicate
   
    0x18E728A7,  //Redesign
     
    0x18E768B7, // new line
    
    0x18E738A7, //from here down are all numbers; this is 0
    
     0x18E748A7,
     
    0x18E768A7,
    
     0x18E778A7,
       
     0x18E788A7,
     
      0x18E798A7,
     
        0x18E718B7,
       
     0x18E728B7,
        
      0x18E738B7,
         
      0x18E758B7,
         
};

IRrecv irrecv(irReceivePin);    // create the IR library
decode_results results;         // IR data goes here

IRsend irsend;

void setup()
{

  Serial.begin(9600);

  pinMode(irReceivePin, INPUT);

  irrecv.enableIRIn();              // Start the IR receiver
 

}

void loop()

{

  long key;
 

  if (irrecv.decode(&results))
  {
    // here if data is received
    irrecv.resume();

//    showReceivedData();
    
    key = convertCodeToKey(results.value);
    
    translateCodes(key);

//    if(key >= 0)
//    {
//      Serial.print("Got ");
//      //Serial.println(key);
//      Serial.println(translatedCodes[key]);
//    }
  }
 
 
 
  
}

//translate the code to a section on the PDC or a number

void translateCodes(long key) {
  
  if (key > 8) {
    Serial.print(key-9);
    
  } else if (key == 0) {
    Serial.print("Define Problem:");
  } else if (key == 1) {
    Serial.print("Research:");
} else if (key == 2) {
    Serial.print("Brainstorm:");
    } else if (key == 3) {
    Serial.print("Select:");
    } else if (key == 4) {
    Serial.print("Construct:");
    } else if (key == 5) {
      Serial.print("Test/Evaluate:");
       } else if (key == 6) {
      Serial.print("Communicate:");
       } else if (key == 7) {
      Serial.print("Redesign:");
      } else if (key == 8) {
      Serial.println();
    } else {
    Serial.println("Unknown Code");
    }
}

/*
 * converts a remote protocol code to a logical key code 
 * (or -1 if no digit received)
 */

int convertCodeToKey(long code)
{

  for( int i=0; i < numberOfKeys; i++)
  {

    if( code == irKeyCodes[i])
    {
      return i; // found the key so return it
    }
  }
  return -1;
}

/*
 * display the protocol type and value
 */

void showReceivedData()
{

  
  if (results.decode_type == UNKNOWN)
  {
    Serial.println("-Could not decode message");
  }

  else
  {

    if (results.decode_type == NEC) {
      Serial.print("- decoded NEC: ");
    }

    else if (results.decode_type == SONY) {
      Serial.print("- decoded SONY: ");
    }

    else if (results.decode_type == RC5) {
      Serial.print("- decoded RC5: ");
    }

    else if (results.decode_type == RC6) {
      Serial.print("- decoded RC6: ");
    }
    
    Serial.print("hex value = ");
    Serial.println( results.value, HEX);

  }

}
