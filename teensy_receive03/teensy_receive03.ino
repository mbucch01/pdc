/*

This sketch decods IR messages using the triplet language

 */

#include <IRremote.h>           // IR remote control library

const int irReceivePin = 10;     // pin connected to IR detector output
const int numberOfKeys = 19;     //  how many keys you want to learn

int value;
int val = 1;
int length;
int index = 1;

long irKeyCodes[numberOfKeys] = {

  // type of information codes, index 0-3
  
  0x18E758A7,  //category

  0x18E708F7,  //time

  0x18E78877,  //user ID

  0x18E748B7,  //project ID
  
  //separator codes, index 4-8

  0x18E7C837,  // comma
  
   0x18E7C8E7, // end of transmission
  
   0x18E718A7, 
   
    0x18E728A7,
    
      0x18E768B7,
    
    0x18E738A7, //from here down are all numbers, this is 0; index 9-18
    
     0x18E748A7,
     
    0x18E768A7,
    
     0x18E778A7,
       
     0x18E788A7,
     
      0x18E798A7,
      
       0x18E718B7,
       
        0x18E728B7,
        
         0x18E738B7,
         
          0x18E758B7,
    
};

IRrecv irrecv(irReceivePin);    // create the IR library
decode_results results;         // IR data goes here

IRsend irsend;

void setup()
{

  Serial.begin(9600);

  pinMode(irReceivePin, INPUT);

  irrecv.enableIRIn();              // Start the IR receiver
 

}

void loop()

{

  long key;
 

  if (irrecv.decode(&results))
  {
    // here if data is received
    irrecv.resume();

//    showReceivedData();
    
    key = convertCodeToKey(results.value);
    
    if (val == 1) {
    translateCodes1(key);
    }

 if (val == 2) {
    translateCodes2(key);
    }
    
     if (val == 3) {
    translateCodes3(key);
    }
    
    val++;
    
    if (val == 4) { 
      val = 1;
    }

  }
 
 
 
  
}

//translate the code to a section on the PDC or a number

void translateCodes1(long key) {
  
  if (key == 0) {
    Serial.print("Category:");
  } else if (key == 1) {
    Serial.print("Time:");
} else if (key == 2) {
    Serial.print("User ID:");
    } else if (key == 3) {
    Serial.print("Project ID:");
    } else {
    Serial.println("Error in 1");
    }
}

void translateCodes2(long key) {
  
  length = key-9;
  Serial.println(length);
}

void translateCodes3(long key) {
  
  if (index < length) {
    Serial.print(key-9);
    index ++;
    val = 2;
   
  } else if (index == length) {
    Serial.println(key-9);
    Serial.println("End of Transmission");
    index = 1;
    
  } else {
    Serial.println("Error in 3");
    index = 1;
}
// if (index == length-1) {
//      index = 0;
//      val = 3;
//      Serial.println("End of Transmission");
//    }
}




/*
 * converts a remote protocol code to a logical key code 
 * (or -1 if no digit received)
 */

int convertCodeToKey(long code)
{

  for( int i=0; i < numberOfKeys; i++)
  {

    if( code == irKeyCodes[i])
    {
      return i; // found the key so return it
    }
  }
  return -1;
}

/*
 * display the protocol type and value
 */

void showReceivedData()
{

  
  if (results.decode_type == UNKNOWN)
  {
    Serial.println("-Could not decode message");
  }

  else
  {

    if (results.decode_type == NEC) {
      Serial.print("- decoded NEC: ");
    }

    else if (results.decode_type == SONY) {
      Serial.print("- decoded SONY: ");
    }

    else if (results.decode_type == RC5) {
      Serial.print("- decoded RC5: ");
    }

    else if (results.decode_type == RC6) {
      Serial.print("- decoded RC6: ");
    }
    
    Serial.print("hex value = ");
    Serial.println( results.value, HEX);

  }

}
