/*

This sketch decods IR messages using the triplet language

 */

#include <IRremote.h>           // IR remote control library

const int irReceivePin = 10;     // pin connected to IR detector output
const int numberOfKeys = 19;     //  how many keys you want to learn
const int maxNumberOfCodes = 20;

int value;
int val = 1;
int length;
long number1;
long number2;
long number;
int thousands;



long transmissionArray[2][maxNumberOfCodes];
int index = 0;

long irKeyCodes[numberOfKeys] = {

  // type of information codes, index 0-4
  
  0x18E758A7,  //category
  0x18E708F7,  //time
  0x18E78877,  //user ID
  0x18E748B7,  //project ID  
   0x18E7C8E7, // end of transmission
   
  //separator codes, index 5-8, so far no use

    0x18E7C837,  // comma
    0x18E718A7,    
    0x18E728A7,
    0x18E768B7,
    
    0x18E738A7, //from here down are all numbers, this is 0; index 9-18
    0x18E748A7,     
    0x18E768A7,    
    0x18E778A7,       
    0x18E788A7,     
    0x18E798A7,      
    0x18E718B7,       
    0x18E728B7,
    0x18E738B7,
    0x18E758B7,    
};

IRrecv irrecv(irReceivePin);    // create the IR library
decode_results results;         // IR data goes here

IRsend irsend;

void setup()
{

  Serial.begin(9600);

  pinMode(irReceivePin, INPUT);

  irrecv.enableIRIn();              // Start the IR receiver
 

}

void loop()

{

  long key;
 

  if (irrecv.decode(&results))
  {
    // here if data is received
    irrecv.resume();

//    showReceivedData();
    
    key = convertCodeToKey(results.value);
    
        
    if (val == 3) {
    translateCodes3(key);
    }
    
    
   if (val == 2) {
    translateCodes2(key);
    }
    
    if (val == 1) {
    translateCodes1(key);
    }


   
    
    if (val == 4) { 
      val = 1;
    }

  }
 
 
 
  
}

//translate the code to a section on the PDC or a number

void translateCodes1(long key) {
  

   if (key == 4) {
     printTransmission();
     val = 4;
   } else if (key > 4) {
     transmissionArray[0][index] = -1;
     Serial.println("Error in 1");
     val = 1;
   }else {
     transmissionArray[0][index] = key;
     val++;
   }
     
}

void translateCodes2(long key) {
  
  length = key-9;
  if (length < 1) {
    Serial.println("Error in 2");
    val = 1;
    number = 0;
  } else {
  Serial.print("length: ");
  Serial.println(length);
  number = 0;
  val++;
  }
}

void translateCodes3(long key) {
  
  if (length >= 0) {
    
    // if the length is four multiply by 1000, if its 3 multiply by 100, etc. then do length = length -1
    // this function should never be called when length = 0, length should always be redefined before that point
    
    length = length-1;
    number1 = key-9;
    for (int i=0; i<length; i++) {
      number1 = number1*10;
    }
    number += number1;
    
    if (length == 0) {
      
      transmissionArray[1][index] = number;
      number = 0;
      index++;
      val++;
    }
    
  } else {
    Serial.println("Error in 3");
    val = 1;
    length = 0;
    number = 0;
}

// if (index == length-1) {
//      index = 0;
//      val = 3;
//      Serial.println("End of Transmission");
//    }
}


//print the array when the end of transmission code is received

void printTransmission() {
  
  for (int i=0; i<index; i++) {
    Serial.print("ID: ");
    Serial.print(transmissionArray[0][i]);
    Serial.print(", ");
    Serial.println(transmissionArray[1][i]);
  }
index = 0;
}



/*
 * converts a remote protocol code to a logical key code 
 * (or -1 if no digit received)
 */

int convertCodeToKey(long code)
{

  for( int i=0; i < numberOfKeys; i++)
  {

    if( code == irKeyCodes[i])
    {
      return i; // found the key so return it
    }
  }
  return -1;
}
