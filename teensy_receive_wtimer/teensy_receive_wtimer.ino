/*

This sketch decodes IR messages written with category/ time pairs.
Then it prints an opening sequence to the computer, followed by a 
character number (sometimes) and all the number pairs.

 */

#include <IRremote.h>           // IR remote control library

const int irReceivePin = 10;     // pin connected to IR detector output
const int numberOfKeys = 11;     //  how many keys you want to learn
const int maxNumberOfCodes = 20;
const String transmission_sequence = "abcdefg";

int value;
int thousands;
int time;
int length;
int val = 1;

int redLED = 1;
int greenLED = 2;

long transmissionArray[2][maxNumberOfCodes];
int index = 0;

IRsend irsend;

long irKeyCodes[numberOfKeys] = {
    0x18E738A7, //from here down are all numbers, this is 0; index 9-18
    0x18E748A7,     
    0x18E768A7,    
    0x18E778A7,       
    0x18E788A7,     
    0x18E798A7,      
    0x18E718B7,       
    0x18E728B7,
    0x18E738B7,
    0x18E758B7,  
    0x18E7C8E7 // end of transmission  
};

IRrecv irrecv(irReceivePin);    // create the IR library
decode_results results;         // IR data goes here



void setup()
{
  
  for(int i=0;i<2;i++) {
   for(int j=0;j<maxNumberOfCodes;j++) {
    transmissionArray[i][j]=0;
   } 
  }

  pinMode(redLED, OUTPUT);
  pinMode(greenLED, OUTPUT);
  Serial.begin(9600);

  pinMode(irReceivePin, INPUT);

  irrecv.enableIRIn();              // Start the IR receiver
 
//  digitalWrite(greenLED, HIGH);

}

void loop()

{

  
  long key;
  
  if (millis()-time>2000) {
    digitalWrite(redLED, LOW);
    digitalWrite(greenLED, HIGH);
  } else {
    digitalWrite(greenLED, LOW);
    digitalWrite(redLED, HIGH);
  }
    
    
 

  if (irrecv.decode(&results))
  {
    // here if data is received
    irrecv.resume();
    
    time = millis();
    digitalWrite(greenLED, LOW);
    digitalWrite(redLED, HIGH);

//    showReceivedData();
    
    key = convertCodeToKey(results.value);
    
    switch (val) {
    case 1: 
      val = translateCodes1(key);
      break;
 
    case 2: 
      val = translateCodes2(key);
      break;
    
    case 3:
      val = translateCodes3(key);
      break;
    }

  }
 
 
 
  
}

//translate the code to a section on the PDC or a number

int translateCodes1(long key) {
  
    digitalWrite(greenLED, LOW);
    digitalWrite(redLED, HIGH);
  

   if (key == 10) {
     printTransmission();
     return 1;
   } else {
     transmissionArray[0][index] = key;
     Serial.print("from 1: ");
     Serial.println(key);
     return 2;
   }
     
}

int translateCodes2(long key) {  
  length = key;
  Serial.print("from 2: ");
  Serial.println(key);
  return 3;
}

int translateCodes3(long key) {
  long number = 0;
  long number1 = 0;
  
  if (length >= 0) {
    
    // if the length is four multiply by 1000, if its 3 multiply by 100, etc. then do length = length -1
    // this function should never be called when length = 0, length should always be redefined before that point
    
    length--;
    number1 = key;
    for (int i=0; i<length; i++) {
      number1 = number1*10;
    }
    number += number1;
    
    if (length == 0) {
      Serial.print("from 3: ");
     Serial.println(number);
      
      transmissionArray[1][index] = number;
      number = 0;
      index++;
      return 1;
    } else {
      return 3;
    }
    
  } else {
    Serial.println("Error in 3");
    return 1;
    length = 0;
    number = 0;
  }

}


//print the array when the end of transmission code is received

void printTransmission() {
  
   Keyboard.print(transmission_sequence);
   delay(10);
   int num_characters = countCharacters(index);
   Keyboard.print("0,");
   delay(10);
   Keyboard.print(num_characters);
   delay(10);   
   Keyboard.print(";");
   delay(10);
  
  for (int i=0; i<index; i++) {
    Keyboard.print(transmissionArray[0][i]);
    delay(10);
    Keyboard.print(",");
    delay(10);
    Keyboard.print(transmissionArray[1][i]);
    delay(10);
    if (i == index-1) {
      Keyboard.print(":");
      delay(10);
    } else {
      Keyboard.print(";");
      delay(10);
    }
    
  }
index = 0;
}

//counts the number of characters in transmissionArray, plus commas and semicolons

int countCharacters(int num_cols)
{
  int num_chars=0;
  num_chars=num_cols*4;    // two numbers per column, one comma/semicolon/colon per number
  return num_chars;
}

/*
 * converts a remote protocol code to a logical key code 
 * (or -1 if no digit received)
 */

int convertCodeToKey(long code)
{

  for( int i=0; i < numberOfKeys; i++)
  {

    if( code == irKeyCodes[i])
    {
      return i; // found the key so return it
    }
  }
  return -1;
}
